import 'dart:ffi';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:massarius_flutter_gad/massarius_flutter_gad.dart';

class PrebidBanner extends StatefulWidget {
  final AdSize thAdSize;
  final String serverAccountId;
  final String configId;
  final String adUnit;
  final void Function(String status) onDemandFetched;
  PrebidBanner(
      {@required this.thAdSize,
      @required this.serverAccountId,
      @required this.configId,
      @required this.adUnit,
      @required this.onDemandFetched});

  @override
  _PrebidBannerState createState() => _PrebidBannerState();
}

class _PrebidBannerState extends State<PrebidBanner> {
  GADBannerViewController _controller;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SizedBox(
            height: widget.thAdSize.height,
            width: widget.thAdSize.width,
            child: _build(context)));
  }

  Widget _build(BuildContext context) {
    if (Platform.isAndroid) {
      return AndroidView(
        viewType: 'plugins.massarius.com/flutter_gad/banner',
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    } else if (Platform.isIOS) {
      return UiKitView(
        viewType: 'plugins.massarius.com/flutter_gad/banner',
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    }
    return null;
  }

  void _onPlatformViewCreated(int id) {
    _controller = GADBannerViewController._internal(
        id: id,
        serverAccountId: widget.serverAccountId,
        thAdSize: widget.thAdSize,
        configId: widget.configId,
        adUnit: widget.adUnit,
        onDemandFetched: widget.onDemandFetched);
    _controller._init();
  }
}

class GADBannerViewController {
  final void Function(GADBannerViewController controller) onAdViewCreated;
  final String serverAccountId;
  final AdSize thAdSize;
  final String configId;
  final String adUnit;
  final void Function(String status) onDemandFetched;

  GADBannerViewController._internal({
    this.onAdViewCreated,
    this.thAdSize,
    this.adUnit,
    this.configId,
    this.serverAccountId,
    this.onDemandFetched,
    int id,
  }) : _channel = MethodChannel(Platform.isIOS
            ? 'plugins.massarius.com/flutter_gad/banner'
            : 'plugins.massarius.com/flutter_gad/banner');

  final MethodChannel _channel;

  Future<void> reload() {
    return _load();
  }

  Future<void> _init() {
    _channel.setMethodCallHandler(_handler);
    return _load();
  }

  Future<void> _load() {
    return _channel.invokeMethod('fetch', {
      "serverAccountId": this.serverAccountId,
      "adHeight": this.thAdSize.height,
      "adWidth": this.thAdSize.width,
      "configId": this.configId,
      "adUnit": this.adUnit,
    });
  }

  Future<void> _handler(MethodCall call) async {
    switch (call.method) {
      case "demandFetched":
        onDemandFetched(call.arguments['name']);
        break;
    }
  }
}
