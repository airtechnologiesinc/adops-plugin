class AdSize {
  final double width;
  final double height;
  AdSize(this.width, this.height);
}
