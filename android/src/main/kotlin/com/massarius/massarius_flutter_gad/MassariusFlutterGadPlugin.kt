package com.massarius.massarius_flutter_gad

import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.util.*
import org.prebid.mobile.*
import org.prebid.mobile.addendum.AdViewUtils
import org.prebid.mobile.addendum.PbFindSizeError

/** MassariusFlutterGadPlugin */
class MassariusFlutterGadPlugin: FlutterPlugin, MethodCallHandler {
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "plugins.massarius.com/flutter_gad/banner")
    //channel.setMethodCallHandler(this)
    channel.setMethodCallHandler { call, result ->
      handle(call, result)
    }
  }

  private fun handle(call: MethodCall, result: Result){
    when (call.method){
      "fetch" -> {this.load(call, result)}
      else -> result.notImplemented()
    }
  }

  private fun load(call: MethodCall, results: Result){
    val argument = call.arguments as Dictionary<String, Any>
    print("printing from swift:::${argument}")

    val publisherId = argument["serverAccountId"] as? String
    val adUnitId = argument["adUnit"] as? String
    val configId = argument["configId"] as? String
    val adHeight = argument["adHeight"] as? Double
    val adWidth = argument["adWidth"] as? Double

    PrebidMobile.setPrebidServerAccountId(PLACEMENT_ACCOUNT_ID)

    PrebidMobile.setPrebidServerHost(Host.APPNEXUS);


    PrebidMobile.setApplicationContext(this)

  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
