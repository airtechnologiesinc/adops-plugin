import 'package:flutter/material.dart';
import 'package:massarius_flutter_gad/massarius_flutter_gad.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: PrebidBanner(
            thAdSize: AdSize(300, 250),
            serverAccountId: "71518ca7-db2d-47de-bfe4-8e9e63f15e65",
            adUnit: "/13436254/Benjamin_test_ATF",
            configId: "06a4032c-322b-4444-943f-02409fb479ff",
            onDemandFetched: (String status) =>
                print("Prebid status: " + status),
          ),
        ),
      ),
    );
  }
}
