package com.massarius.massarius_flutter_gad_example

import android.content.Context
import android.graphics.Color
import android.view.View
import io.flutter.plugin.platform.PlatformView

internal class PrebidBannerView(context: Context, id: Int, creationParams: Map<String, Any>?) : PlatformView {
    private val mainView: View = View(context)
    private val PLACEMENT_ACCOUNT_ID: String = "71518ca7-db2d-47de-bfe4-8e9e63f15e65"
    private val PLACEMENT_CONFIG_ID: String = "e7604548-4e5d-4ebc-8f2d-db30e2aeafec"
    private val PLACEMENT_AD_WIDTH: Int = 300
    private val PLACEMENT_AD_HEIGHT: Int = 250
    private val ADUNIT_ID: String = "/13436254/simon_app_test_ATF"


    override fun getView(): View {
        return mainView
    }

    override fun dispose() {}

    init {
        mainView.setBackgroundColor(Color.rgb(255, 255, 255))
    }
}