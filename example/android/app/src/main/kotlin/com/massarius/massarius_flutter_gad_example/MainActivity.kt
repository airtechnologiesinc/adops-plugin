package com.massarius.massarius_flutter_gad_example

import android.os.Bundle
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherAdView
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine

class MainActivity: FlutterActivity() {

    private lateinit var mPublisherAdView : PublisherAdView

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        flutterEngine.platformViewsController.registry.registerViewFactory("plugins.massarius.com/flutter_gad/banner", NativeViewFactory())

        setContentView(R.layout.activity_main)

        mPublisherAdView = findViewById(R.id.pbBanner)
        val adRequest = PublisherAdRequest.Builder().build()
        mPublisherAdView.loadAd(adRequest)
    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//    }
}
