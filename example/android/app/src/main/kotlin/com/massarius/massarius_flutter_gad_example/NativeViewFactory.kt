package com.massarius.massarius_flutter_gad_example

import android.content.Context
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory
//import com.massarius.massarius_flutter_gad_example.PrebidBannerView

internal class NativeViewFactory : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
    override fun create(context: Context, id: Int, args: Any?): PlatformView {
        val creationParams = args as Map<String, Any>?
        return PrebidBannerView(context, id, creationParams)
    }
}