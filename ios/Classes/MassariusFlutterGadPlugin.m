#import "MassariusFlutterGadPlugin.h"
#if __has_include(<massarius_flutter_gad/massarius_flutter_gad-Swift.h>)
#import <massarius_flutter_gad/massarius_flutter_gad-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "massarius_flutter_gad-Swift.h"
#endif

@implementation MassariusFlutterGadPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftMassariusFlutterGadPlugin registerWithRegistrar:registrar];
}
@end
