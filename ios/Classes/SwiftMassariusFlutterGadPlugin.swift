import Flutter
import UIKit

public class SwiftMassariusFlutterGadPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    registrar.register(PrebidBannerViewFactory(messenger: registrar.messenger()), withId: "plugins.massarius.com/flutter_gad/banner")
    
    let channel = FlutterMethodChannel(name: "massarius_flutter_gad", binaryMessenger: registrar.messenger())
    //registrar.register(PrebidBannerViewFactory(messenger: registrar.messenger()), withId: "plugins.massarius.com/flutter_gad/banner")
    let instance = SwiftMassariusFlutterGadPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
