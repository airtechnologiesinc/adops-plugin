//
//  PrebidBannerView.swift
//  massarius_flutter_gad
//
//  Created by Benjamin Acquah on 27/03/2021.
//

import UIKit
import GoogleMobileAds
import PrebidMobile

class PrebidBannerView: NSObject,FlutterPlatformView {
    private var _view: UIView
    var channel: FlutterMethodChannel!
    var adUnit: AdUnit!
    var amBanner: GADBannerView!
    
    init(frame: CGRect,viewIdentifier viewId: Int64,arguments args: Any?,binaryMessenger messenger: FlutterBinaryMessenger?) {
        _view = UIView()
        channel = FlutterMethodChannel(name: "plugins.massarius.com/flutter_gad/banner", binaryMessenger: messenger!)
        super.init()
        channel.setMethodCallHandler { (call, result) in
            self.handle(call: call, result: result)
        }
    }
    
    func view() -> UIView {
        return self._view
    }
    
    private func handle(call: FlutterMethodCall, result: @escaping FlutterResult){
        switch call.method {
        case "fetch":
            self.load(call, result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    private func load(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let argument = call.arguments as! Dictionary<String, Any>
        print("printing from swift:::",argument)
        
        let publisherId = argument["serverAccountId"] as? String ?? ""
        let adUnitId = argument["adUnit"] as? String ?? ""
        let configId = argument["configId"] as? String ?? ""
        let adHeight = argument["adHeight"] as? Double ?? 0
        let adWidth = argument["adWidth"] as? Double ?? 0

        Prebid.shared.prebidServerAccountId = publisherId
        Prebid.shared.prebidServerHost = .Appnexus
        let adSize = CGSize(width: adWidth, height: adHeight)
        self.adUnit = BannerAdUnit(configId: configId, size: adSize)
        self.adUnit.setAutoRefreshMillis(time: 60000)

        self.amBanner = GADBannerView(adSize: GADAdSizeFromCGSize(adSize))
        self.amBanner.adUnitID = adUnitId
        self.amBanner.rootViewController = UIApplication.shared.delegate!.window!!.rootViewController!
        self.amBanner.delegate = self
        addBannerViewToView(bannerView: amBanner)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = [(kGADSimulatorID as! String)]

        let request = GADRequest()

        adUnit.fetchDemand(adObject: request) { (resultCode) in
            self.channel.invokeMethod("demandFetched", arguments: ["name": resultCode.name()])
            print("PreBid demand fetch for GAD\(resultCode.name())")
            self.amBanner.load(request)
        }
        
    }
    
    func addBannerViewToView(bannerView: GADBannerView){
        AdViewUtils.findPrebidCreativeSize(bannerView) { (size) in
            guard let bannerView = bannerView as? GADBannerView else { return }
            bannerView.adSize = GADAdSizeFromCGSize(size)
        } failure: { (error) in
            print("error: \(error.localizedDescription)")
        }
        
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        self.view().addSubview(bannerView)
        self.view().addConstraints([NSLayoutConstraint(item: bannerView,
                                                        attribute: .centerX,
                                                        relatedBy: .equal,
                                                        toItem: self.view(),
                                                        attribute: .centerX,
                                                        multiplier: 1,
                                                        constant: 0),
                                     NSLayoutConstraint(item: bannerView,
                                                        attribute: .centerY,
                                                        relatedBy: .equal,
                                                        toItem: self.view(),
                                                        attribute: .centerY,
                                                        multiplier: 1,
                                                        constant: 0)])
    }
}

extension PrebidBannerView: GADBannerViewDelegate {
    
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidRecievedAd")
    }
    
    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
}


class PrebidBannerViewFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger

    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        super.init()
    }

    func create(withFrame frame: CGRect,viewIdentifier viewId: Int64,arguments args: Any?) -> FlutterPlatformView {
        return PrebidBannerView(frame: frame,viewIdentifier: viewId,arguments: args,binaryMessenger: messenger)
    }
}
